<?php
/**
 * Template Name: Home Slideshow
 *
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( array( 'page-homeslideshow.twig' ), $context );